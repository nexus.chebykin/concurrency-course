#pragma once

#include <function2/function2.hpp>

namespace exe::tp {

// Intrusive tasks?
using Task = fu2::unique_function<void()>;

}  // namespace exe::tp
